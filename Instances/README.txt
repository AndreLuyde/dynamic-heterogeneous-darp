The format of data is as follows:

The first line contains the following information:

	k n cp

where

	k = number of vehicles

	n = number of customers

	cp = number of collective pickup place.


The next k lines contain, for each vehicle, the following information:

    D q1 q2 q3 q4 C

where

	D = maximum duration of a route

	q1 = numbers of seats for companion
	
	q2 = numbers of seats for simple patient
	
	q3 = numbers of seats for stretcher patient
	
	q4 = numbers of seats for wheelchair patient
	
	C = cost for use the vehicle


The next cp lines contain, for each collective pickup place, the following information:
	
	i x y
	
where

	i = pickup place number

	x = x coordinate

	y = y coordinate


The next (n*2) lines contain the customers, where the first n are pickup customers and the next n are deliver customers. 
Every pickup customer has its equivalent in delivery customers. Example:
The pickup customer of line 2 has the correspondent delivery customer in line (2+n).
The next (n*2) lines contain, for each customer, the following information:

	i x y st dt q1 q2 q3 q4 e l

where

	i = customer number

	x = x coordinate

	y = y coordinate

	st = service time
	
	dt = maximum ride time

	q1 = number of seats required by companions
	
	q2 = number of seats required by simple patients
	
	q3 = number of seats required by stretcher patients
	
	q4 = number of seats required by wheelchair patients
	
	e = beginning of time window

	l = end of time window
